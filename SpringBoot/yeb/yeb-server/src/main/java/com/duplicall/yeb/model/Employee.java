package com.duplicall.yeb.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_employee")
@ApiModel(value="Employee对象", description="")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "员工编号")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "员工姓名")
    private String name;

    @ApiModelProperty(value = "性别")
    private String gender;

    @ApiModelProperty(value = "出生日期")
    private LocalDate birthday;

    @ApiModelProperty(value = "身份证号")
    private String id_card;

    @ApiModelProperty(value = "婚姻状况")
    private String wedlock;

    @ApiModelProperty(value = "民族")
    private Integer nation_id;

    @ApiModelProperty(value = "籍贯")
    private String native_place;

    @ApiModelProperty(value = "政治面貌")
    private Integer politic_id;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "电话号码")
    private String phone;

    @ApiModelProperty(value = "联系地址")
    private String address;

    @ApiModelProperty(value = "所属部门")
    private Integer department_id;

    @ApiModelProperty(value = "职称ID")
    private Integer job_level_id;

    @ApiModelProperty(value = "职位ID")
    private Integer pos_id;

    @ApiModelProperty(value = "聘用形式")
    private String engage_form;

    @ApiModelProperty(value = "最高学历")
    private String tiptop_degree;

    @ApiModelProperty(value = "所属专业")
    private String specialty;

    @ApiModelProperty(value = "毕业院校")
    private String school;

    @ApiModelProperty(value = "入职日期")
    private LocalDate begin_date;

    @ApiModelProperty(value = "在职状态")
    private String work_state;

    @ApiModelProperty(value = "工号")
    private Integer work_id;

    @ApiModelProperty(value = "合同期限")
    private Double contract_term;

    @ApiModelProperty(value = "转正日期")
    private LocalDate conversion_time;

    @ApiModelProperty(value = "离职日期")
    private LocalDate not_work_date;

    @ApiModelProperty(value = "合同起始日期")
    private LocalDate begin_contract;

    @ApiModelProperty(value = "合同终止日期")
    private LocalDate end_contract;

    @ApiModelProperty(value = "工龄")
    private String work_age;

    @ApiModelProperty(value = "工资账套ID")
    private Integer salary_id;


}
