package com.duplicall.yeb.service;

import com.duplicall.yeb.model.Position;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface IPositionService extends IService<Position> {

}
