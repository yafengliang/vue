package com.duplicall.yeb.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_employee_ec")
@ApiModel(value="EmployeeEc对象", description="")
public class EmployeeEc implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    @ApiModelProperty(value = "员工编号")
    private Integer eid;

    @ApiModelProperty(value = "奖罚日期")
    private LocalDateTime ec_date;

    @ApiModelProperty(value = "奖罚原因")
    private String ec_reason;

    @ApiModelProperty(value = "奖罚分")
    private String ec_point;

    @ApiModelProperty(value = "奖罚类别，0：奖，1：罚")
    private String ec_type;

    @ApiModelProperty(value = "备注")
    private String remark;


}
