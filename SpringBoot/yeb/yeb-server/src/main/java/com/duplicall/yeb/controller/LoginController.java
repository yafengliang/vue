package com.duplicall.yeb.controller;

import com.duplicall.yeb.model.Admin;
import com.duplicall.yeb.model.pojo.AdminLoginParams;
import com.duplicall.yeb.service.IAdminService;
import com.duplicall.yeb.utils.RespBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * @author yafengliang@yeah.net
 * @Description 登录
 * @date 2021/5/18 9:32
 */
@RestController
@Api(tags = "LoginController")
public class LoginController {

    @Autowired
    private IAdminService adminService;

    @ApiOperation(value = "登录之后返回Token")
    @PostMapping(value = "/login")
    public RespBean login(@RequestBody AdminLoginParams adminLoginParams, HttpServletRequest request){
        return adminService.login(adminLoginParams.getUsername(),adminLoginParams.getPassword(),adminLoginParams.getCode(),request);
    }

    @ApiOperation(value = "获取当前登录用户信息")
    @GetMapping(value = "/user/info")
    public Admin getAdminInfo(Principal principal){
        if (null ==principal){
            return null;
        }
        String username = principal.getName();
        Admin admin = adminService.getAdminByUserName(username);
        admin.setPassword(null);
        return admin;
    }

    @ApiOperation(value = "退出登录")
    @PostMapping(value = "/logout")
    public RespBean logout(){
        return RespBean.success("注销成功!");
    }

}
