package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.Appraise;
import com.duplicall.yeb.mapper.AppraiseMapper;
import com.duplicall.yeb.service.IAppraiseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class AppraiseServiceImpl extends ServiceImpl<AppraiseMapper, Appraise> implements IAppraiseService {

}
