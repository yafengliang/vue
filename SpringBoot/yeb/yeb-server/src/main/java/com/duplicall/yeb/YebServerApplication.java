package com.duplicall.yeb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yafengliang@yeah.net
 * @Description
 * @date 2021/5/14 9:57
 */
@SpringBootApplication
@MapperScan("com.duplicall.yeb.mapper")
public class YebServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(YebServerApplication.class, args);
    }
}
