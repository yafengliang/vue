package com.duplicall.yeb.mapper;

import com.duplicall.yeb.model.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}
