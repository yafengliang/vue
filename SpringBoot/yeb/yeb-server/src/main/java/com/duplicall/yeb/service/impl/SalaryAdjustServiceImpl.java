package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.SalaryAdjust;
import com.duplicall.yeb.mapper.SalaryAdjustMapper;
import com.duplicall.yeb.service.ISalaryAdjustService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class SalaryAdjustServiceImpl extends ServiceImpl<SalaryAdjustMapper, SalaryAdjust> implements ISalaryAdjustService {

}
