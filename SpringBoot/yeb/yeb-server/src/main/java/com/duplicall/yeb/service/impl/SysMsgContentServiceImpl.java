package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.SysMsgContent;
import com.duplicall.yeb.mapper.SysMsgContentMapper;
import com.duplicall.yeb.service.ISysMsgContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class SysMsgContentServiceImpl extends ServiceImpl<SysMsgContentMapper, SysMsgContent> implements ISysMsgContentService {

}
