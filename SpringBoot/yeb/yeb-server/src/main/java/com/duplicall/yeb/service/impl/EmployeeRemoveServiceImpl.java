package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.EmployeeRemove;
import com.duplicall.yeb.mapper.EmployeeRemoveMapper;
import com.duplicall.yeb.service.IEmployeeRemoveService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class EmployeeRemoveServiceImpl extends ServiceImpl<EmployeeRemoveMapper, EmployeeRemove> implements IEmployeeRemoveService {

}
