package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.Role;
import com.duplicall.yeb.mapper.RoleMapper;
import com.duplicall.yeb.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
