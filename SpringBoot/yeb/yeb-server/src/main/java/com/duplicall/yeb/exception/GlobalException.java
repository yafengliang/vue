package com.duplicall.yeb.exception;

import com.duplicall.yeb.utils.RespBean;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @author yafengliang@yeah.net
 * @Description 全局异常处理
 * @date 2021/5/27 15:26
 */
@RestControllerAdvice
public class GlobalException {

    @ExceptionHandler(SQLException.class)
    public RespBean mySqlException(SQLException sqlException){
        if (sqlException instanceof SQLIntegrityConstraintViolationException){
            return RespBean.error("该数据又关联数据，操作失败");
        }
        return RespBean.error("数据库异常,操作异常");
    }
}
