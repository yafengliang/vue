package com.duplicall.yeb.service;

import com.duplicall.yeb.model.Admin;
import com.baomidou.mybatisplus.extension.service.IService;
import com.duplicall.yeb.utils.RespBean;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface IAdminService extends IService<Admin> {

    /**
     *  登录后返回Token
     * @param username
     * @param password
     * @param code
     * @param request
     * @return
     */
    RespBean login(String username, String password, String code, HttpServletRequest request);

    /**
     *  获取当前登录的用户名
     * @param username
     * @return
     */
    Admin getAdminByUserName(String username);


}
