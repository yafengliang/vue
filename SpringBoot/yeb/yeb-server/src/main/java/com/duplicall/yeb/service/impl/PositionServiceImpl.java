package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.Position;
import com.duplicall.yeb.mapper.PositionMapper;
import com.duplicall.yeb.service.IPositionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class PositionServiceImpl extends ServiceImpl<PositionMapper, Position> implements IPositionService {

}
