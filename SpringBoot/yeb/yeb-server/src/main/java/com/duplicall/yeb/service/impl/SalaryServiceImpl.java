package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.Salary;
import com.duplicall.yeb.mapper.SalaryMapper;
import com.duplicall.yeb.service.ISalaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class SalaryServiceImpl extends ServiceImpl<SalaryMapper, Salary> implements ISalaryService {

}
