package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.PoliticsStatus;
import com.duplicall.yeb.mapper.PoliticsStatusMapper;
import com.duplicall.yeb.service.IPoliticsStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class PoliticsStatusServiceImpl extends ServiceImpl<PoliticsStatusMapper, PoliticsStatus> implements IPoliticsStatusService {

}
