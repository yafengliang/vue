package com.duplicall.yeb.controller;


import com.duplicall.yeb.model.Menu;
import com.duplicall.yeb.service.IAdminService;
import com.duplicall.yeb.service.IMenuService;
import com.duplicall.yeb.utils.RespBean;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@RestController
@RequestMapping("/system/config")
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @ApiOperation(value = "通过用户ID查询菜单列表")
    @GetMapping(value = "/menu")
    public List<Menu> getMenuByAdminId() {
        return menuService.getMenuByAdminId();
    }
}
