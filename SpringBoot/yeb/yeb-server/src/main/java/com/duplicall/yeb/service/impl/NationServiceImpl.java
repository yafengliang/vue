package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.Nation;
import com.duplicall.yeb.mapper.NationMapper;
import com.duplicall.yeb.service.INationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class NationServiceImpl extends ServiceImpl<NationMapper, Nation> implements INationService {

}
