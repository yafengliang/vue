package com.duplicall.yeb.mapper;

import com.duplicall.yeb.model.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface MenuMapper extends BaseMapper<Menu> {
    /**
     * 根据用户ID获取菜单列表
     * @param id
     * @return
     */
    List<Menu> getMenuByAdminId(Integer id);

    /**
     * 查询所有菜单
     * @return
     */
    List<Menu> getAllMenus();
}
