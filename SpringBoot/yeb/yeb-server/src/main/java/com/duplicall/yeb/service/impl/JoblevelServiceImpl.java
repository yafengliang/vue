package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.Joblevel;
import com.duplicall.yeb.mapper.JoblevelMapper;
import com.duplicall.yeb.service.IJoblevelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class JoblevelServiceImpl extends ServiceImpl<JoblevelMapper, Joblevel> implements IJoblevelService {

}
