package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.EmployeeTrain;
import com.duplicall.yeb.mapper.EmployeeTrainMapper;
import com.duplicall.yeb.service.IEmployeeTrainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class EmployeeTrainServiceImpl extends ServiceImpl<EmployeeTrainMapper, EmployeeTrain> implements IEmployeeTrainService {

}
