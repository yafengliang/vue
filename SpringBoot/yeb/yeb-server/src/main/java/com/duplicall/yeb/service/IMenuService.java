package com.duplicall.yeb.service;

import com.duplicall.yeb.model.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface IMenuService extends IService<Menu> {

    /**
     *  根据用户ID获取菜单列表
     * @param
     * @return
     */
    List<Menu> getMenuByAdminId();

    /**
     *  查询所有菜单
     * @return
     */
    List<Menu> getAllMenu();
}
