package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.Oplog;
import com.duplicall.yeb.mapper.OplogMapper;
import com.duplicall.yeb.service.IOplogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class OplogServiceImpl extends ServiceImpl<OplogMapper, Oplog> implements IOplogService {

}
