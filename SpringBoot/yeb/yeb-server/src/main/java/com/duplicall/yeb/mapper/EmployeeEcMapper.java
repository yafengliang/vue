package com.duplicall.yeb.mapper;

import com.duplicall.yeb.model.EmployeeEc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface EmployeeEcMapper extends BaseMapper<EmployeeEc> {

}
