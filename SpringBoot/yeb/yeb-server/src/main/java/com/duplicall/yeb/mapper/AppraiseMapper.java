package com.duplicall.yeb.mapper;

import com.duplicall.yeb.model.Appraise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface AppraiseMapper extends BaseMapper<Appraise> {

}
