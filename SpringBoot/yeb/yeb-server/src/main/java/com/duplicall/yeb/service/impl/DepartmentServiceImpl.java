package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.Department;
import com.duplicall.yeb.mapper.DepartmentMapper;
import com.duplicall.yeb.service.IDepartmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

}
