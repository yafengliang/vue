package com.duplicall.yeb.mapper;

import com.duplicall.yeb.model.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.duplicall.yeb.model.Menu;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface AdminMapper extends BaseMapper<Admin> {



}
