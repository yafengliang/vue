package com.duplicall.yeb.controller;


import com.duplicall.yeb.model.Joblevel;
import com.duplicall.yeb.model.Position;
import com.duplicall.yeb.service.IJoblevelService;
import com.duplicall.yeb.service.IPositionService;
import com.duplicall.yeb.utils.RespBean;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@RestController
@RequestMapping("/sys/basic/jobLevel")
@Slf4j
public class JoblevelController {

    @Autowired
    private IJoblevelService jobLevelService;

    @ApiOperation(value = "获取所有职称信息")
    @GetMapping(value = "/")
    public List<Joblevel> getAllJobLevel(){
        log.info("获取所有职称信息");
        return jobLevelService.list();
    }

    @ApiOperation(value = "添加职称信息")
    @PostMapping(value = "/")
    public RespBean addJobLevel(@RequestBody Joblevel joblevel){
        joblevel.setCreate_date(LocalDateTime.now());
        if (jobLevelService.save(joblevel)){
            return RespBean.success("添加成功");
        }else {
            return RespBean.error("添加失败");
        }
    }

    @ApiOperation(value = "更新职称信息")
    @PutMapping(value = "/")
    public RespBean updateJobLevel(@RequestBody Joblevel joblevel){
        if (jobLevelService.updateById(joblevel)){
            return RespBean.success("更新成功");
        }else {
            return RespBean.error("更新失败");
        }
    }

    @ApiOperation(value = "删除职称信息")
    @DeleteMapping(value = "/{id}")
    public RespBean deleteJobLevel(@PathVariable(value = "id") Integer id){
        if (jobLevelService.removeById(id)){
            return RespBean.success("删除成功");
        }else {
            return RespBean.error("删除失败");
        }
    }

    @ApiOperation(value = "批量删除职称信息")
    @DeleteMapping(value = "/")
    public RespBean deleteJobLevelByIds(Integer[] ids){
        if (jobLevelService.removeByIds(Arrays.asList(ids))){
            return RespBean.success("批量删除成功");
        }
        return RespBean.success("批量删除失败");
    }

}
