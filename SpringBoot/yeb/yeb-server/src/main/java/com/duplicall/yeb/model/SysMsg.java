package com.duplicall.yeb.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_sys_msg")
@ApiModel(value="SysMsg对象", description="")
public class SysMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    @ApiModelProperty(value = "消息id")
    private Integer msgid;

    @ApiModelProperty(value = "0表示群发消息")
    private String type;

    @ApiModelProperty(value = "这条消息是给谁的")
    private String sendid;

    @ApiModelProperty(value = "0 未读 1 已读")
    private String state;


}
