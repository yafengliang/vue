package com.duplicall.yeb.service;

import com.duplicall.yeb.model.MenuRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.duplicall.yeb.utils.RespBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
public interface IMenuRoleService extends IService<MenuRole> {

    /**
     * 更新角色菜单
     * @param rid
     * @param mids
     * @return
     */
    RespBean updateMenuRole(Integer rid, Integer[] mids);
}
