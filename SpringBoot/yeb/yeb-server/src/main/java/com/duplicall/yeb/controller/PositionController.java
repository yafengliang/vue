package com.duplicall.yeb.controller;


import com.duplicall.yeb.model.Position;
import com.duplicall.yeb.service.IPositionService;
import com.duplicall.yeb.utils.RespBean;
import io.swagger.annotations.ApiOperation;
import javafx.geometry.Pos;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@RestController
@RequestMapping("/sys/basic/pos")
@Slf4j
public class PositionController {

    @Autowired
    private IPositionService positionService;

    @ApiOperation(value = "获取所有职位信息")
    @GetMapping(value = "/")
    public List<Position> getAllPosition(){
        log.info("获取所有职位信息");
        return positionService.list();
    }

    @ApiOperation(value = "添加职位信息")
    @PostMapping(value = "/")
    public RespBean addPosition(@RequestBody Position position){
        position.setCreate_date(LocalDate.now());
        if (positionService.save(position)){
            return RespBean.success("添加成功");
        }else {
            return RespBean.error("添加失败");
        }
    }

    @ApiOperation(value = "更新职位信息")
    @PutMapping(value = "/")
    public RespBean updatePosition(@RequestBody Position position){
        if (positionService.updateById(position)){
            return RespBean.success("更新成功");
        }else {
            return RespBean.error("更新失败");
        }
    }

    @ApiOperation(value = "删除职位信息")
    @DeleteMapping(value = "/{id}")
    public RespBean deletePosition(@PathVariable(value = "id") Integer id){
        if (positionService.removeById(id)){
            return RespBean.success("删除成功");
        }else {
            return RespBean.error("删除失败");
        }
    }

    @ApiOperation(value = "批量删除职位信息")
    @DeleteMapping(value = "/")
    public RespBean deletePositionByIds(Integer[] ids){
        if (positionService.removeByIds(Arrays.asList(ids))){
            return RespBean.success("批量删除成功");
        }
        return RespBean.success("批量删除失败");
    }

}
