package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.SysMsg;
import com.duplicall.yeb.mapper.SysMsgMapper;
import com.duplicall.yeb.service.ISysMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class SysMsgServiceImpl extends ServiceImpl<SysMsgMapper, SysMsg> implements ISysMsgService {

}
