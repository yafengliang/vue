package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.mapper.AdminMapper;
import com.duplicall.yeb.model.Admin;
import com.duplicall.yeb.model.Menu;
import com.duplicall.yeb.mapper.MenuMapper;
import com.duplicall.yeb.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {
    @Autowired
    private MenuMapper menuMapper;
    /**
     *  根据用户ID获取菜单列表
     * @param
     * @return
     */
    @Override
    public List<Menu> getMenuByAdminId() {
        return menuMapper.getMenuByAdminId(((Admin) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
    }

    @Override
    public List<Menu> getAllMenu() {

        return menuMapper.getAllMenus();
    }
}
