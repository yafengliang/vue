package com.duplicall.yeb.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.duplicall.yeb.model.Menu;
import com.duplicall.yeb.model.MenuRole;
import com.duplicall.yeb.model.Role;
import com.duplicall.yeb.service.IMenuRoleService;
import com.duplicall.yeb.service.IMenuService;
import com.duplicall.yeb.service.IRoleService;
import com.duplicall.yeb.utils.RespBean;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yafengliang@yeah.net
 * @Description 角色管理
 * @date 2021/5/31 15:53
 */
@RestController
@RequestMapping(value = "/system/basic/permission")
public class PermissionController {

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IMenuService menuService;

    @Autowired
    private IMenuRoleService menuRoleService;

    @ApiOperation(value = "获取所有角色")
    @GetMapping(value = "/")
    public List<Role> getAllRoles(){
        return roleService.list();
    }


    @ApiOperation(value = "添加角色")
    @PostMapping(value = "/role")
    public RespBean addRole(@RequestBody Role role){
        if (!role.getName().startsWith("ROLE_")){
            role.setName("ROLE_"+role.getName());
        }
        if (roleService.save(role)){
            return RespBean.success("添加成功!");
        }
        return RespBean.error("添加失败！");
    }


    @ApiOperation(value = "/删除角色")
    @DeleteMapping(value = "/role/{rid}")
    public RespBean deleteRole(@PathVariable Integer rid){
        if (roleService.removeById(rid)){
            return RespBean.success("删除成功！");
        }
        return RespBean.error("删除失败！");
    }

    @ApiOperation(value = "查询所有菜单")
    @GetMapping(value = "/menus")
    public List<Menu> getAllMenus(){
        return menuService.getAllMenu();
    }

    @ApiOperation(value = "根据角色ID查询菜单")
    @GetMapping(value = "/mid/{rid}")
    public List<Integer> getMidBiRid(@PathVariable Integer rid){
        return menuRoleService.list(new QueryWrapper<MenuRole>().eq("rid",rid)).stream().map(MenuRole::getMid).collect(Collectors.toList());
    }

    @ApiOperation(value = "更新角色菜单")
    @PutMapping(value = "/")
    public RespBean updateMenuRole(Integer rid,Integer[] mids ){
        return menuRoleService.updateMenuRole(rid,mids);
    }

}
