package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.EmployeeEc;
import com.duplicall.yeb.mapper.EmployeeEcMapper;
import com.duplicall.yeb.service.IEmployeeEcService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class EmployeeEcServiceImpl extends ServiceImpl<EmployeeEcMapper, EmployeeEc> implements IEmployeeEcService {

}
