package com.duplicall.yeb.service.impl;

import com.duplicall.yeb.model.MailLog;
import com.duplicall.yeb.mapper.MailLogMapper;
import com.duplicall.yeb.service.IMailLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yafengliang
 * @since 2021-05-14
 */
@Service
public class MailLogServiceImpl extends ServiceImpl<MailLogMapper, MailLog> implements IMailLogService {

}
