let proxyObj = {}

proxyObj["/"] = {
    //websocket
    ws: false,
    //代理指向
    target: "http://localhost:8090",
    //发送请求头host会被设置成target
    changeOrigin: true,
    //不重写后端请求地址
    pathRewrite: {
        '^/': "/"
    }
}

module.exports = {
    devServer: {
        host: "localhost",
        port: 8080,
        proxy: proxyObj
    }
}